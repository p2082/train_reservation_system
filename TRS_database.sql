create database TRS_db;

use TRS_db;

create table schedule(id int primary key auto_increment,arrival_time varchar(20),departure_time varchar(20),train_id int,route_id int,arrival_date date,departure_date date);

create table booking(id int primary key auto_increment,name varchar(100),age int,gender varchar(20),mobile varchar(20),booking_type varchar(255),booking_status varchar(255),booking_date date,departure_date date,fare_amount double,seat_number varchar(20),ticket_count int,train_id int,route_id int,user_id int);

create table train(id int primary key auto_increment,train_name varchar(50),train_number varchar(50) unique,train_type varchar(50),total_seats int,available_seats int,fare_price double,route_id int);

create table route(id int primary key auto_increment,destination varchar(50),start_point varchar(50));

create table payment(id int primary key auto_increment,payable_amount double,card_number varchar(255),payment_method varchar(255),status varchar(255),transaction_date date,booking_id int,user_id int);

create table user(id int primary key auto_increment,birthdate date,email varchar(50) unique,gender varchar(50),mobile varchar(255),name varchar(255),password varchar(255),role varchar(255));

ALTER TABLE schedule ADD FOREIGN KEY (train_id) REFERENCES train(id);

ALTER TABLE schedule ADD FOREIGN KEY (route_id) REFERENCES route(id);

ALTER TABLE booking ADD FOREIGN KEY (train_id) REFERENCES train(id);

ALTER TABLE booking ADD FOREIGN KEY (route_id) REFERENCES route(id);

ALTER TABLE booking ADD FOREIGN KEY (user_id) REFERENCES user(id);

ALTER TABLE train ADD FOREIGN KEY (route_id) REFERENCES route(id);

ALTER TABLE payment ADD FOREIGN KEY (user_id) REFERENCES user(id);

ALTER TABLE payment ADD FOREIGN KEY (booking_id) REFERENCES booking(id);

insert into user values (1, '1995-03-21', 'admin@gmail.com', 'male', '9898989898', 'Admin', '$2a$10$sxT3G9oPJuGCZCcL5v5wyu9xREzE12wNQf7uqk6jXTR3/WRVv9uMe', 'admin');